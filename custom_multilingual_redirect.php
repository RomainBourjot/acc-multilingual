<?php

/*
Plugin Name: Custom Multilingual Redirect
Plugin URI:  https://developer.wordpress.org/plugins/the-basics/
Description: Manage automatic HTTP multilingual redirections.
Version:     2.1
Author:      Romain Bourjot
License:     All rights reserved
*/


/*

	Users are redirected only once at the beginning of session, meaning when they enter the site.
	Sesssion are only time-based (time defined by $acc_timer in seconds).
	Same principle as Google Analytics sessions except here they are only time-based :
	https://support.google.com/analytics/answer/2731565?hl=en#time-based-expiration

	The language in which to translate the page is found by looking into the browser's ACCEPT_LANGUAGE.
	This plugin partly uses code that has been public and tested for long from :
	http://www.thefutureoftheweb.com/blog/use-accept-language-header

	Redirection is made by 302 redirect.
	See : https://developers.google.com/webmasters/mobile-sites/mobile-seo/separate-urls#using-http-redirects

	GET url paramters are kepts after redirect:
	http://accengage.com/it/?utm_source=foobar -> http://accengage.com/fr/utm_source=foobar 
*/


//Security check
defined('ABSPATH') or die('[...]');

global $acc_production, $acc_to_log, $acc_timer;

$acc_production = true;  // If false => will output JS logging
$acc_to_log = array();    // Array for JS logging always place: 'global $acc_to_log;' on top of a function if you want to allow logging
$acc_timer = 30*60;       // DEFAULT DURATION OF THE SESSION IN SECONDS

/**
 * Preflight check for redirect.
 * Not allowable if (any):
 *  * inside adminisation panel
 *  * WPML is not loaded
 *  * WPML language is not defined
 *  * HTTP_ACCEPT_LANGUAGE is not defined (mainly bots)
 *
 * @return true if redirection is allowable, false otherwise.
 */
function acc_multilingual_init_check() {
	return !is_admin() && //Check that we are not in backoffice
			defined('ICL_LANGUAGE_CODE') && // Check that WPML is loaded and current language is defined
			isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && // Check that HTTP_ACCPET_LANGUAGE is present (avoid bots)
			$_SERVER['HTTP_ACCEPT_LANGUAGE'] !== ''; // Check that HTTP_ACCPET_LANGUAGE is present (avoid bots)
}

/**
 * Checks if the current user can be redirect (ie. not in a session).
 *
 * @return True if they can be redirected, false otherwise
 */
function acc_multilingual_handle_session() {
	global $acc_to_log,$acc_timer;


	$duration = get_option('acc_multilingual_session_duration');


	if ($duration !== false && intval($duration) > 0) { $acc_timer = $duration*60; }

	array_push($acc_to_log,"Session duration: ".$acc_timer."min");

	$to_ret = true;

	if (isset($_COOKIE['acc_multilingual_redirect'])) {
		// A cookie has been found
		$cookie_timestamp = $_COOKIE['acc_multilingual_redirect'];
		array_push($acc_to_log,"Cookie found: ".$cookie_timestamp);

		if (is_object($cookie_timestamp)) {
			//If cookie_timestamp is an object, the next intval($cookie_timestamp) evaluation will output an E_NOTICE and make the plugin crash and deactivate.
			array_push($acc_to_log,"Cookie is an object! [and that is really bad]");
		} else {
			$cookie_timestamp = intval($cookie_timestamp); // Converting to integer

			if (!is_int($cookie_timestamp)) {
				array_push($acc_to_log,"Cookie is not an integer! [and that is bad]");
				$to_ret = true; // INVALID COOKIE, AUTHORIZING REDIRECT
			}  else {
				if (($cookie_timestamp + $acc_timer) < time()) { // This is a new session
					array_push($acc_to_log,"New Session!");
					$to_ret = true; // Authorize redirect
				} else { // Still in a session
					array_push($acc_to_log,"Still in session");
					$to_ret = false; // Don't authorize redirect
				}
			}
		}

	} else {
		array_push($acc_to_log,"No cookie found");
	}

	//Relaunching timer
	setcookie('acc_multilingual_redirect', time()-5, time()+86400, COOKIEPATH, COOKIE_DOMAIN ); //-5s to avoid conflicts when loading multiple pages during the same second

	array_push($acc_to_log,"Relaunching timer...");
	array_push($acc_to_log,"It is: ".date("h\h i\m s\s"));
	array_push($acc_to_log,"Session expires at: ".date("h\h i\m s\s", time()+$acc_timer));

	return $to_ret;
}


/**
 * Finds the preferred languages of the user in order,
 * then finds the first of the languages for which we have transaltions.
 *
 * @return {String} Empty if no language find, a language in the form 'en', 'fr', 'de' otherwise.
 * @see http://www.thefutureoftheweb.com/blog/use-accept-language-header Source
 */
function acc_multilingual_read_http_accept() {
  global $acc_to_log;
	$def_lang = '';
	$langs = array();

	if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
		// break up string into pieces (languages and q factors)
		preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse);

		if (count($lang_parse[1])) {
			// create a list like "en" => 0.8
			$langs = array_combine($lang_parse[1], $lang_parse[4]);

			// set default to 1 for any without q factor
			foreach ($langs as $lang => $val) {
				if ($val === '') $langs[$lang] = 1;
			}

			// sort list based on value
			arsort($langs, SORT_NUMERIC);
		}
	}

	// Cycle thourgh the preferred languages and use the first one that matches our languages
	foreach ($langs as $lang => $val) {
		if (strlen($lang) > 2) {
			$lang = substr($lang, 0, 2);
		}

    // Checks that we know that language
		if (apply_filters('wpml_language_is_active', NULL, $lang)) {
			$def_lang = $lang;
			break;
		}
	}

	return $def_lang;
}





// JavaScript logging for debug with console.log, only activated if $acc_production = false
// XSS safe
//
/**
 * Logs strings in the JavaScript console.
 * XSS safe
 *
 * Usage:
 * global $acc_to_log
 * [...]
 * array_push($acc_to_log,string_to_log);
 *
 */
function acc_multilingual_log() {
	global $acc_to_log,$acc_production;
	if ($acc_production) { return; }

	echo '<script>';

	foreach ($acc_to_log as $v) {
		echo 'console.log("'.preg_replace( "/\r|\n/", "",htmlentities($v, ENT_QUOTES)).'");';
	}

	echo '</script>';
}





/**
 * Main function.
 *
 */
function acc_multilingual_redirect() {
	global $acc_to_log, $sitepress;

	array_push($acc_to_log,"Custom Multilingual Plugin Loaded");

	if (!acc_multilingual_init_check($query)) {//Check init conditions
		array_push($acc_to_log,"Init conditions not met, exiting plugin...");
		return;
	}

	array_push($acc_to_log,"Init conditions met...");

	if (!acc_multilingual_handle_session()) { // CHECK IF SESSION HANDLER AUTHORIZE REDIRECT
		array_push($acc_to_log,"Redirection not authorized by session handler, exiting...");
		return;
	} else {
		array_push($acc_to_log,"Redirection authorized by session handler");
	}


	$browser_lang = strtolower(acc_multilingual_read_http_accept()); // Try to find browser language, to lower case for future comparison
	if ($browser_lang === "") { array_push($acc_to_log,"Unable to detect browser language, exiting..."); return; }
	array_push($acc_to_log,'Browser Lang found: '.$browser_lang);

	$current_lang = strtolower(apply_filters('wpml_current_language', NULL)); // Find current page language, to lower case for comparison
	if ($browser_lang === $current_lang) { array_push($acc_to_log,"Aleady in target language, exiting..."); return; } // If already in target language, return and do nothing


	$current_uri = '//'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; // Looking for current page id
	$current_id = url_to_postid($current_uri);

	array_push($acc_to_log,"URI: ".$current_uri);
	array_push($acc_to_log,"Page/Post Id: ".$current_id );
	array_push($acc_to_log,"Current lang: ".$current_lang);


	$translated_id = apply_filters('wpml_object_id', $current_id, get_post_type($current_id), false, $browser_lang); // Looking for a translation of the current page

	if ($translated_id === NULL) { array_push($acc_to_log,"No translation found, exiting..."); return; }
	array_push($acc_to_log,"Translated id : ".$translated_id);

	if ($translated_id === $current_id) { array_push($acc_to_log,"Translation and current page have same id, exiting..."); return; }


	$sitepress->switch_lang($browser_lang, true); // Changing system lang to target language
	$translated_url = get_permalink($translated_id); // Getting permalink in target language
	array_push($acc_to_log,"Translated permalink: ".$translated_url);
	$sitepress->switch_lang($current_lang, true); // Changing system lang to original language, probably useless since exit just after, but in doubt I prefer to.


	if ($translated_url === false) { // Unable to find the permalink
		array_push($acc_to_log,"Unable to find translated permalink, exiting...");
		return;
	}


	array_push($acc_to_log, "Looking for get parameters");
	array_push($acc_to_log, 'REQUEST URI:');
	array_push($acc_to_log, var_export($_SERVER['REQUEST_URI'], true));

	$interrogation_position = strpos($_SERVER['REQUEST_URI'], '?'); // find the position of the first '?' in the URI
	$trailing_str = null;

	if ($interrogation_position === FALSE) {
		array_push($acc_to_log, 'No arguments found');
	} else {
		array_push($acc_to_log, 'Arguments found at'.$interrogation_position);
		array_push($acc_to_log, 'Trailing: '.-(strlen($_SERVER['REQUEST_URI'])-$interrogation_position));

		$trailing_str = substr($_SERVER['REQUEST_URI'], -(strlen($_SERVER['REQUEST_URI'])-$interrogation_position)); // get the param string : '?foo=bar&bar=foobar'
		array_push($acc_to_log, $trailing_str);
	}


	// REDIRECT 302 see -> https://developers.google.com/webmasters/mobile-sites/mobile-seo/separate-urls#using-http-redirects
	if (wp_redirect($translated_url.$trailing_str, 302)) { // try to redirect
    	exit; // if redirection is okay, die
	}
}

add_action('after_setup_theme', 'acc_multilingual_redirect'); // Hooks the main function
add_action('wp_head', 'acc_multilingual_log'); // Hooks the JS logging system

add_action('init','add_acc_ml_queries');










/* ***************************************************************************************** */
/* ***************************************************************************************** */
/*                                                                                           */
/*                                   ADMINISTRATION PANEL                                    */
/*                                                                                           */
/* ***************************************************************************************** */
/* ***************************************************************************************** */
/* ***************************************************************************************** */









add_action('admin_menu', 'acc_multilingual_setup_menu'); // adds the administration panel to the settings menu

function acc_multilingual_setup_menu() { // options
    add_options_page( 'Custom Multilingual Redirect', 'ACC ML Redirection', 'manage_options', 'ACC ML Redirection', 'acc_multilingual_admin' );
}





function acc_multilingual_admin() {

	$acc_ml_duration = get_option('acc_multilingual_session_duration');

	if ($acc_ml_duration === false) { $acc_ml_duration = 30; }


	if (isset($_POST['acc_multilingual_session_duration']) && intval($_POST['acc_multilingual_session_duration']) > 0) {
		update_option('acc_multilingual_session_duration', intval($_POST['acc_multilingual_session_duration']));
		$acc_ml_duration = $_POST['acc_multilingual_session_duration'];
	}

	?>
	<div class="wrap">
		<h1>Accengage Multilingual Redirection</h1>

		<form name="form1" method="post" action="">

			<p>
				Durée d'une session (en minutes):
				<input type="text" name="acc_multilingual_session_duration" value="<?php echo $acc_ml_duration; ?>" size="20">
			</p>

			<p class="submit">
				<input type="submit" name="Submit" class="button-primary" value="Sauvegarder" />
			</p>

		</form>

	</div>
	<?php
}
